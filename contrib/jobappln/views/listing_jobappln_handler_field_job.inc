<?php
/**
 * @file
 * Contains the basic job field handler.
 */

/**
 * Field handler to provide simple renderer that allows linking to a node.
 */
class listing_jobappln_handler_field_job extends views_handler_field {
  /**
   * Constructor to provide additional field to add.
   */
  function construct() {
    parent::construct();
    $this->additional_fields['job_nid'] = 'job_nid';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_node'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Provide link to node option
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_node'] = array(
      '#title' => t('Link this field to its node'),
      '#description' => t('This will override any other link you have set.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_node']),
    );
  }

  /**
   * Render whatever the data is as a link to the node.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    //drupal_set_message($values->{$this->aliases['field_listing_jobappln_job_value']});
	$this->options['alter']['alter_text'] = TRUE;
	$this->options['alter']['text'] = db_result(db_query("SELECT title FROM {node} WHERE nid=%d", $values->{$this->aliases['job_nid']}));
    if (!empty($this->options['link_to_node']) && $data !== NULL && $data !== '') {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "node/" . $values->{$this->aliases['job_nid']};
    }
    return $data;
  }

  function render($values) {
    return $this->render_link(check_plain($values->{$this->field_alias}), $values);
  }
}
