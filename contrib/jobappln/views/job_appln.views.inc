<?php

/**
 * Implementation of hook_views_data():
 * Present fields and filters for user data.
 */
function job_appln_views_data() {

  $data['job_appln']['table']['group']  = t('Job Application');
  $data['job_appln']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Job Application'),
    'help' => t('Job Application Views 2 handlers.'),
  );
  $data['job_appln']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['job_appln']['job_nid'] = array(
   'title' => t('Job'),
    'help' => t('Job being applied for.'),
    'field' => array(
      'handler' => 'job_appln_handler_field_job',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'job_appln_handler_argument_job_nid',
      'parent' => 'views_handler_argument_numeric', // make sure parent is included
      'name field' => 'title', // the field to display in the summary.
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    // Information for sorting on a nid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
 //  $data['node']['table']['group']  = t('Job Application');
   $data['job_appln']['view_applications'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to applications made for this job.'),
      'handler' => 'job_appln_handler_field_job_link',
    ),
  );

  $data['job_appln']['uid'] = array(
   'title' => t('Job Applicant'),
    'help' => t('User who made the application.'),
	'operator' =>  array('=' => 'equals'),
    'field' => array(
      'handler' => 'job_appln_handler_field_user_name',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'title' => t('Uid'),
      'handler' => 'views_handler_filter_user_current',
    ),
   );
	

  return $data;
}


function job_appln_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'job_appln') .'/views',
    ),
    'handlers' => array(
      // field handlers
      'job_appln_views_handler_field_applicant' => array(
        'parent' => 'views_handler_field',
      ),
      'job_appln_handler_field_user' => array(
        'parent' => 'views_handler_field',
      ),
      'job_appln_handler_field_user_name' => array(
        'parent' => 'job_appln_handler_field_user',
      ),
      'job_appln_handler_field_job' => array(
        'parent' => 'views_handler_field',
      ),
      'job_appln_handler_argument_job_nid' => array(
        'parent' => 'views_handler_argument_numeric',
      ),
      'job_appln_handler_field_job_link' => array(
        'parent' => 'views_handler_field',
      ),
    )
  );
}

