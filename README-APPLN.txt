README.txt

INSTALLATION

Install as usual.  jobappln requires the job module.
Apply the patch if necessary otherwise jobappln will not work
jobappln creates a content type called jobappln.  The default content type includes three fields: status, brief and comments.  The status field is meant to be kept so that employers can grade the applicants according to suitability and filter using views.



DEFAULT VIEWS

MyApplications - all job applications belonging to the current user grouped by job.  Note that it would be sensible to create a role for employers and add it to this view so that the link to this view is not shown to employees.

EmployerApplications - all applications made to jobs created by the current user

JobApplications - all applications made the current job.  This takes an argument and appears as a link to the job creator on a job node view

