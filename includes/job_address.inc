<?php
   if(db_table_exists('content_type_job')) {
      db_query("ALTER TABLE {content_type_job} 
  ADD field_job_address_is_primary tinyint(4) default '0',
  ADD field_job_address_aname varchar(75) default '',
  ADD field_job_address_country varchar(2) default 'us',
  ADD field_job_address_province varchar(16) default '',
  ADD field_job_address_city varchar(255) default '',
  ADD field_job_address_street varchar(255) default '',
  ADD field_job_address_additional varchar(255) default '',
  ADD field_job_address_postal_code varchar(16) default ''");

   }
   else db_query("
CREATE TABLE {content_type_job} (
  vid int(10) unsigned NOT NULL default '0',
  nid int(10) unsigned NOT NULL default '0',
  field_job_address_is_primary tinyint(4) default '0',
  field_job_address_aname varchar(75) default '',
  field_job_address_country varchar(2) default 'us',
  field_job_address_province varchar(16) default '',
  field_job_address_city varchar(255) default '',
  field_job_address_street varchar(255) default '',
  field_job_address_additional varchar(255) default '',
  field_job_address_postal_code varchar(16) default '',
  PRIMARY KEY  (vid),
  KEY nid (nid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8"  
   );

db_query("INSERT INTO {content_node_field_instance} (field_name, type_name, weight, label, widget_type, widget_settings, display_settings, description, widget_module, widget_active) VALUES ('%s', '%s', %d, '%s', '%s', '%s', '%s', '%s', '%s', %d)",
('field_job_address', 'job', 3, 'Location', 'addresses_elements', 'a:2:{s:13:"default_value";a:1:{i:0;a:7:{s:10:"additional";s:0:"";s:6:"street";s:0:"";s:5:"aname";s:0:"";s:11:"postal_code";s:0:"";s:4:"city";s:0:"";s:5:"phone";s:0:"";s:3:"fax";s:0:"";}}s:17:"default_value_php";N;}', 'a:7:{s:5:"label";a:2:{s:6:"format";s:5:"above";s:7:"exclude";i:0;}s:6:"teaser";a:2:{s:6:"format";s:7:"default";s:7:"exclude";i:0;}s:4:"full";a:2:{s:6:"format";s:7:"default";s:7:"exclude";i:0;}i:4;a:2:{s:6:"format";s:7:"default";s:7:"exclude";i:0;}i:2;a:2:{s:6:"format";s:7:"default";s:7:"exclude";i:0;}i:3;a:2:{s:6:"format";s:7:"default";s:7:"exclude";i:0;}s:5:"token";a:2:{s:6:"format";s:7:"default";s:7:"exclude";i:0;}}', '', 'addresses_cck', 1));

db_query("INSERT INTO {content_node_field} (field_name, type, global_settings, required, multiple, db_storage, module, db_columns, active, locked) VALUES ('%s', '%s', '%s', %d, %d, %d, '%s', '%s', %d, %d)",
('field_job_address', 'addresses_cck', 'a:11:{s:10:"is_primary";s:0:"";s:5:"aname";s:1:"1";s:7:"country";s:1:"0";s:8:"province";s:1:"1";s:4:"city";s:1:"2";s:6:"street";s:1:"1";s:10:"additional";s:1:"1";s:11:"postal_code";s:1:"1";s:5:"phone";s:1:"1";s:3:"fax";s:1:"1";s:6:"#theme";s:0:"";}', 0, 0, 1, 'addresses_cck', 'a:10:{s:10:"is_primary";a:8:{s:4:"type";s:3:"int";s:4:"size";s:4:"tiny";s:5:"title";s:24:"Primary Address Checkbox";s:11:"description";s:54:"Mark it as the primary address or not (default is not)";s:7:"default";i:0;s:7:"display";i:0;s:5:"theme";a:2:{s:5:"plain";s:57:""False" if the checkbox is not checked, otherwise "True".";s:5:"hcard";s:59:"An hcard/vcard html representation of the primary checkbox.";}s:5:"token";s:17:"addresses_general";}s:5:"aname";a:8:{s:4:"type";s:7:"varchar";s:6:"length";i:75;s:5:"title";s:12:"Address Name";s:11:"description";s:73:"The nickname of this address, like "Home", "Office", "Anna''s appartment."";s:7:"default";s:0:"";s:7:"display";i:0;s:5:"theme";a:2:{s:5:"plain";s:17:"The address name.";s:5:"hcard";s:55:"An hcard/vcard html representation of the address name.";}s:5:"token";s:17:"addresses_general";}s:7:"country";a:8:{s:4:"type";s:7:"varchar";s:6:"length";i:2;s:5:"title";s:7:"Country";s:11:"description";s:58:"The ISO alpha 3 code of each country (its a 2-digit code).";s:7:"default";s:2:"us";s:7:"display";i:1;s:5:"theme";a:6:{s:4:"name";s:24:"The name of the country.";s:5:"code2";s:25:"The 2-digit country code.";s:5:"code3";s:25:"The 3-digit country code.";s:10:"name_hcard";s:50:"An hcard/vcard representation of the country name.";s:11:"code2_hcard";s:58:"An hcard/vcard representation of the 2-digit country code.";s:11:"code3_hcard";s:58:"An hcard/vcard representation of the 3-digit country code.";}s:5:"token";s:13:"addresses_adr";}s:8:"province";a:8:{s:4:"type";s:7:"varchar";s:6:"length";i:16;s:5:"title";s:8:"Province";s:11:"description";s:53:"The name of the state, province, county or territory.";s:7:"default";s:0:"";s:7:"display";i:1;s:5:"theme";a:4:{s:4:"name";s:18:"The province name.";s:4:"code";s:18:"The province code.";s:10:"name_hcard";s:51:"An hcard/vcard representation of the province name.";s:10:"code_hcard";s:51:"An hcard/vcard representation of the province code.";}s:5:"token";s:13:"addresses_adr";}s:4:"city";a:8:{s:4:"type";s:7:"varchar";s:6:"length";i:255;s:5:"title";s:4:"City";s:11:"description";s:21:"The name of the city.";s:7:"default";s:0:"";s:7:"display";i:1;s:5:"theme";a:2:{s:5:"plain";s:9:"The city.";s:5:"hcard";s:42:"An hcard/vcard representation of the city.";}s:5:"token";s:13:"addresses_adr";}s:6:"street";a:8:{s:4:"type";s:7:"varchar";s:6:"length";i:255;s:5:"title";s:6:"Street";s:11:"description";s:17:"Street, number...";s:7:"default";s:0:"";s:7:"display";i:1;s:5:"theme";a:2:{s:5:"plain";s:24:"The street, number, etc.";s:5:"hcard";s:44:"An hcard/vcard representation of the street.";}s:5:"token";s:13:"addresses_adr";}s:10:"additional";a:8:{s:4:"type";s:7:"varchar";s:6:"length";i:255;s:5:"title";s:10:"Additional";s:11:"description";s:82:"Additional address information like appartment block, number or address reference.";s:7:"default";s:0:"";s:7:"display";i:1;s:5:"theme";a:2:{s:5:"plain";s:31:"Additional address information.";s:5:"hcard";s:68:"An hcard/vcard representation of the additional address information.";}s:5:"token";s:13:"addresses_adr";}s:11:"postal_code";a:8:{s:4:"type";s:7:"varchar";s:6:"length";i:16;s:5:"title";s:11:"Postal code";s:11:"description";s:49:"The address postal code (ZIP code for US people).";s:7:"default";s:0:"";s:7:"display";i:1;s:5:"theme";a:2:{s:5:"plain";s:16:"The postal code.";s:5:"hcard";s:49:"An hcard/vcard representation of the postal code.";}s:5:"token";s:13:"addresses_adr";}s:5:"phone";a:8:{s:4:"type";s:7:"varchar";s:6:"length";i:24;s:5:"title";s:5:"Phone";s:11:"description";s:44:"Phone numbers, like cellphone, home, office.";s:7:"default";s:0:"";s:7:"display";i:0;s:5:"theme";a:2:{s:5:"plain";s:17:"The phone number.";s:5:"hcard";s:55:"An hcard/vcard html representation of the phone number.";}s:5:"token";s:17:"addresses_general";}s:3:"fax";a:8:{s:4:"type";s:7:"varchar";s:6:"length";i:24;s:5:"title";s:3:"Fax";s:11:"description";s:11:"Fax number.";s:7:"default";s:0:"";s:7:"display";i:1;s:5:"theme";a:2:{s:5:"plain";s:15:"The fax number.";s:5:"hcard";s:53:"An hcard/vcard html representation of the fax number.";}s:5:"token";s:17:"addresses_general";}}', 1, 0));
