Job Plus Drupal 5.x

REQUIREMENTS

Version 5.x
This module provides additional functionality to the job module at project/jobsearch
It requires job, addresses_cck, activeselect and country_select (created to provide state/province select function for addresses_cck using activeselect).  This is available at http://drupal.org/project/addresses)
For full functioning also download adv_taxonomy menu which is available at http://drupal.org/project/adv_taxonomy_menu.  This provides the extra menu functions available on the demo sites, whereby you can filter down through several levels of categories.

Example live sites can be found at http://www.dermpedia.org/jobs and http://jobs.gutpedia.org

An example live site can be found at http://www.crewcentral.com

Other Requirements:
Currently this module only works for one job content type which is called 'job'

**********************************************************************************************************************************************************************

INSTALLATION

- Install required modules as normal
- Create categories for jobs and configure adv_taxonomy_menu if being used
- Configure jobplus settings
- Make sure that job type is selected 

-----------------------------------
Settings for 5.x 
- configure country_select for default country
- add a cck_address field to your job content type and configure it as follows
  'Free text Entry
  Allow other countries  (Since this system overrides the default cck_address functions we do not need to select any countries here (only Canada and US available anyway)
  Select at least state and country fields to use
  Ignore select from DB
------------------------------------
Settings for 6.x 
- Configure addresses module and select countries to include but deactivate the province field.  The country/province select only works if this is deactivated
- Add an addresses field to your job content type
------------------------------------

- Create a home page to use for jobs, and enter the url in 'admin/settings/job/jobplus_settings'
- Enable blocks for countries, province/states and for adv_taxonomy_menu menu
- Create jobs and have lots of fun!


**********************************************************************************************************************************************************************

FEATURES

- Built in Views for jobs by country and jobs by province/state
- Descriptive breadcrumb
- Any jobs with no country selected are reported here: 'admin/settings/job/countries'
- Filter jobs by many levels of categorization using adv_taxonomy menu
