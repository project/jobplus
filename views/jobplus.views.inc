<?php

/**
 * Implementation of hook_views_handlers().
 */
function jobplus_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'jobplus') .'/views',
    ),
    'handlers' => array(
      'jobplus_handler_bycountry' => array(
        'parent' => 'views_handler_argument',
      ),
      'jobplus_handler_byprovince' => array(
        'parent' => 'views_handler_argument',
      ),
     
    ),
  );
}


// ---------- Table OG

function jobplus_views_data() {
  $data['content_type_job']['table']['group']  = t('jobplus');

  $data['content_type_job']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Node'),
    'help' => t('Job listing.'),
  );

  // joins
  $data['content_type_job']['table']['join'] = array(
    //...to the node table
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['content_type_job']['nid'] = array(
    'title' => t('Jobs by Country'),
    'help' => t('Select jobs by country.'),
	'operator' =>  array('=' => 'equals'),
    'argument' => array(
      'handler' => 'jobplus_handler_bycountry',
    ),
   );
  $data['content_type_job']['field_job_address_province'] = array(
    'title' => t('Jobs by Province'),
    'help' => t('Select jobs by province.'),
	'operator' =>  array('=' => 'equals'),
    'argument' => array(
      'handler' => 'jobplus_handler_byprovince',
    ),
   );
 
   return $data;
}
?>