Jobsearch Plus full job site implementation Drupal 6.x
This module is an extension of the jobsearch module

INSTALLATION
Required modules:

cck - required for addresses field

Enable the following modules:

jobappln - optional -  if present
resume
jobsearch - this creates the job and resume content types, adds country and province fields to the job content type and creates the blocks and breadcrumbs for country, province/state search
addresses - this supplies the country and province fields
adv_taxonomy_menu - this enables you to create a menu system to search on a heirarchy of vocabularies
country_select - this is a wrapper for addresses that enables javascript country, province/state selection
activeselect - the javascript functionality for the country, province/state selection

recommended:
path - required to create a jobs landing page.  This can be customized at admin/settings/job/jobplus_settings

1. jobsearch creates the job content type.  Once created you need to add an addresses field called job_address to the job content type with both the country and state/province field deactivated.  This is because Country Select replaces them.
2. Through the taxonomy system create at least two vocabularies relevant to the job listings you wish to publish.
3. At admin/settings/adv_taxonomy_menu create a menu system out of those two vocabularies.
4. Select the job content type at admin/settings/job and ther resume content tupe at admin/settings/resume
5. Create some jobs and select location and category data.  At admin/build/block enable the block associated with the menu system created above.  The jobs should now display.
6.  Enable the country and province blocks and these should display relevant data.  The breadcrumbs will also display.
7.  Enable the job type block you created using adv_taxonomy_menu and/or the jobplus Jobs by Type.  The block created using adv_taxonomy_menu will remain the same from one page to the other.  The jobplus Jobs by Type block will change dynamically depending on the province and country.

AVAILABLE BLOCKS
Countries - lists countries with the number of jobs in each country in brackets
Provinces - lists the provinces/states with the number of jobs in each province/state in brackets.  This should only appear when a country is actively selected
Jobs by Type: Dynamic - lists the categories with the number of jobs in each category that are in the currently selected country.  Again this shold only appear when a country is actively selected.  Note that the adv_taxonomy_menu module will also create a Jobs by Type block, which is static and displays all jobs in all categories.  The dynamic block uses the same script to create a block 'on the fly' which is not stored in the database.

VIEWS
When using adv_taxonomy_menu you can create a View to use for the category listings.  To create a consistent display you will also need to change the views used for the country and province listings.

CONFLICTS
Country Select will not work correctly when content field permissions is enabled and needs custom handling


JobPlus Application (if present)
This module is a rewrite of the job module that makes applications into nodes enabling them to be manipulated in views.  It makes the author of the application the job creator so that a field can be used in the content type for making notes which can then be displayed with views.  Another field can be created for selecting on short lists and so on...
This module is designed to be used INSTEAD OF the job module.  To disable job applications via the job module untick 'apply for jobs' in job module permissions and/or unselect content types in admin/settings/jobs.  JopPlus Application assumes the job content type is called 'job' as created by JobPlus module.  It will only work with a job content type called 'job'.